import { Response, Router } from "express"
import { getInfoFollow, getInfoReport } from "../controllers/index.controller"

export default () => {
  const route = Router()
  route.get("/", (_,res: any) => res.json({ message: "This is api v1 For Loop" }))
  route.post("/getInfoReport", getInfoReport)
  route.post("/getInfoFollow", getInfoFollow)

  return route
}