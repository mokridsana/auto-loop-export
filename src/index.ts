import * as dotenv from "dotenv"

dotenv.config()

import "reflect-metadata"
import * as express from "express"
import * as bodyParser from "body-parser"
import * as cookieParser from "cookie-parser"
import route from "./routes/index.routes"
import * as cors from "cors"
import * as morgan from "morgan"

const port = process.env.PORT || 8080
// create express app
const app = express()

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
// parse application/json
app.use(bodyParser.json())
// parse an HTML body into a string
app.use(bodyParser.text({ type: 'text/plain', defaultCharset: 'utf-8'}))

app.use(cookieParser())
app.use(morgan("dev"))

// start express server
app.listen(port)

app.use(cors({ origin: true, credentials: true }))

app.use("/", route())

console.log(`Express server loop has started on port ${port}.`)