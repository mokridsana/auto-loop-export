import { NextFunction, Request, Response } from "express";
import api from "../api/api";
import { URLSearchParams } from 'url';
import * as fs from 'fs';
import * as XLSX from 'xlsx';

export const getInfoReport = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const {
            page,
            size,
            startDate,
            endDate,
            username,
            sortBy,
            sortMode,
            sellerIds,
            authen
        } = req.body
        const path = '/api/v1/report/agent/player/transactions/v3'
        let data = {
            page,
            size,
            startDate,
            endDate,
            username,
            sortBy,
            sortMode,
            sellerIds
        }
        let dataReturn = []
        console.log("FIRST STEP");
        let response = await api.callCompanyReport(path, data, authen)
        console.log("TOTAL PAGE ===>", response.data.totalPages);
        for(let i = 0; i < response.data.totalPages + 1; i++){
            console.log(i);
            data.page = i
            response = await api.callCompanyReport(path, data, authen)
            dataReturn.push(response.data.content)
        }
        const mergedArrayConcat = [].concat(...dataReturn);
        const ws = XLSX.utils.json_to_sheet(mergedArrayConcat);
        const wb = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws, `Sheet1`);
        const outputFilePath = `${startDate}-${endDate}.xlsx`;
        XLSX.writeFile(wb, outputFilePath);
        return res.json("SUCCESS")
    } catch (error) {
        console.log("ERROR ===>", error);
        return res.json(error.response.data)
    }
}

export const getInfoFollow = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const {
            page,
            startDate,
            endDate,
            size,
            filterBy,
            sellerId,
            keySearch,
            authen
        } = req.body
        const path = '/api/v1/auth/agent/getListPlayerFollowDepositAmount'
        let data = {
            page,
            startDate,
            endDate,
            size,
            filterBy,
            sellerId,
            keySearch,
            authen
        }
        let dataReturn = []
        console.log("FIRST STEP");
        let response = await api.callCompanyFollow(path, data, authen)
        console.log("TOTAL PAGE ===>", response.data.totalPages);
        for(let i = page; i < response.data.totalPages + 1; i++){
            console.log(i);
            data.page = i
            response = await api.callCompanyFollow(path, data, authen)
            dataReturn.push(response.data.content)
        }
        const mergedArrayConcat = [].concat(...dataReturn);
        const ws = XLSX.utils.json_to_sheet(mergedArrayConcat);
        const wb = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws, `Sheet1`);
        const outputFilePath = `${startDate}-${endDate}.xlsx`;
        XLSX.writeFile(wb, outputFilePath);
        return res.json("SUCCESS")
    } catch (error) {
        console.log("ERROR ===>", error);
        return res.json(error.response.data)
    }
}