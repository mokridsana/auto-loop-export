import axios from "axios"

const endpoint_report = process.env.ENDPOINT_REPORT || ''
const endpoint_follow = process.env.ENDPOINT_FOLLOW || ''

let headersTiger = {
    'content-type': 'application/json',
    'Signature': 'E2CB4D296843D55BBAA8E19988E7E',
    'tiger-system-x-authorization': '',
}

export default {
    callCompanyReport: async (path: string, data: any, authen: string) => {
        headersTiger["tiger-system-x-authorization"] = authen
        return axios.get(`${endpoint_report}${path}`, { params: data, headers: headersTiger }).then(response => response)
    },
    callCompanyFollow: async (path: string, data: any, authen: string) => {
        headersTiger["tiger-system-x-authorization"] = authen
        return axios.get(`${endpoint_follow}${path}`, { params: data, headers: headersTiger }).then(response => response)
    }
}